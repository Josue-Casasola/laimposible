<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Shop Isle
 */
?>
<?php do_action( 'shop_isle_before_footer' ); ?>

	<footer class="footer bg-dark">
		<!-- Divider -->
		<hr class="divider-d">
		<!-- Divider -->
		<div class="container">

			<div class="row">

				<div class="col-sm-6">
					<p class="shop-isle-poweredby-box">
						<a class="shop-isle-poweredby" href="http://laimposible.xyz/" rel="nofollow" target="_blank">Cooperativa de Consumo La Imposible </a> powered by <a class="shop-isle-poweredby"  rel="nofollow">Code Bad Bull</a> <br>
						<a class="shop-isle-poweredby-box">Copyright © 2018 Cooperativa de Consumo La Imposible. All Rights Reserved.</a>
					</p>
				</div>				
			</div><!-- .row -->

		</div>
	</footer>
	<!-- Wrapper end -->
	
	<!-- Scroll-up -->
	<div class="scroll-up">
		<a href="#totop"><i class="arrow_carrot-2up"></i></a>
	</div>

	<?php do_action( 'shop_isle_after_footer' ); ?>

<?php wp_footer(); ?>

</body>
</html>
