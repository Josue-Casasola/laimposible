<?php 

	function imposible_styles(){


		//Register styles
		wp_register_style('desktop' , get_stylesheet_directory_uri().'/css/desktop.css', array(), '1.0');
		// wp_register_style('style' , get_template_directory_uri().'/css/style.css', array('normalize'), '4.2');
		// wp_register_style('bootstrap' , get_template_directory_uri().'/css/bootstrap.css', array(), '3.3.7');
		// wp_register_style('owl-carousel' , get_template_directory_uri().'/css/owl.carousel.min.css', array(), '3.3.7');
		// wp_register_style('owl-theme' , get_template_directory_uri().'/css/owl.theme.default.css', array(), '3.3.7');
		

		//Register scripts
		wp_register_script('jquery' , 'https://code.jquery.com/jquery-2.2.4.js' , array() , '2.2.4');
		// wp_register_script('bootstrapjs' , get_template_directory_uri(). '/js/bootstrap.js' , array() , '3.3.7');
		wp_register_script('greensock' , 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TimelineMax.min.js' , array() , '3.3.7');
		wp_register_script('plugins' , 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/CSSPlugin.min.js');
		wp_register_script('timeline' , 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TimelineLite.min.js');
		wp_register_script('timemax' , 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js' , array() , '3.3.7');
		wp_register_script('scrollmagic' , 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js');
		wp_register_script('debugscroll' , 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js');
		wp_register_script('scrollanimation' , 'http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js');
		wp_register_script('main' , get_stylesheet_directory_uri().'/js/main.js');
		

		//Run styles
		wp_enqueue_style('desktop');
		// wp_enqueue_style('bootstrap');
		// wp_enqueue_style('style');
		// wp_enqueue_style('owl-carousel');
		// wp_enqueue_style('owl-theme');
		

		//Run scripts
		wp_enqueue_script('jquery');
		
		wp_enqueue_script('main');
		wp_enqueue_script('greensock');
		wp_enqueue_script('plugins');
		wp_enqueue_script('timeline');
		wp_enqueue_script('timemax');
		wp_enqueue_script('scrollmagic');
		wp_enqueue_script('debugscroll');
		wp_enqueue_script('jqueryback');
		wp_enqueue_script('owlcarousel');
		
	}

	add_action('wp_enqueue_scripts','imposible_styles');


	//Creación de Menus


	remove_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary' , 'woocommerce_template_single_price', 1 );

	add_filter( 'loop_shop_per_page', 'productos_por_pagina', 20);

	function productos_por_pagina($columnas){
		$columnas = 16;
		return $columnas;
	}
	
	
 ?>